<?php

/**
 * Implements hook_views_data().
 */
function views_ctools_dropdown_views_data() {
  $data['views']['ctools_dropdown'] = array(
    'title' => t('Dropdown links'),
    'help' => t('Displays fields in a dropdown list, like on the views listing page.'),
    'field' => array(
      'handler' => 'views_handler_field_ctools_dropdown',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function views_ctools_dropdown_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_ctools_dropdown') . '/handlers',
    )
    ,
    handlers => array(
      // The name of my handler
      'views_handler_field_links' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_ctools_dropdown' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field_links',
      ),
    ),
  );
}